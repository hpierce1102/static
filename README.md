static
======

A simple static webserver.

Usage
-----
```
npx @haydenpierce/static
```

Development
-----------

```
$ npm install
$ docker-compose up
```