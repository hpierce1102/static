#!/usr/bin/env node

const http = require('http');
const fs = require('fs').promises;
const path = require('path');
const directoryController = require('./src/directoryController');
const fileController = require('./src/fileController');
const pug = require('pug');
const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers')
const argv = yargs(hideBin(process.argv)).argv

const server = http.createServer();

function getContentType(fileExtension) {
    switch (fileExtension) {
        case '.json':
            return 'application/json';
        case '.html':
            return 'text/html'
        case '.js':
            return 'text/javascript'
        default:
            console.warn(`Unknown file extention ${fileExtension}`)
            return 'text/html';
    }
}

server.on('request', async (req, res) => {
    console.log(`${req.method} ${req.url}`);

    try {
        const file = path.resolve(path.join(process.cwd(), req.url));
        const stat = await fs.lstat(file);

        if (stat.isDirectory()) {
            const data = await directoryController(req);

            const templateFunction = pug.compileFile(`${__dirname}/src/directoryController.pug`);
            console.log(`200 OK ${req.url}`);
            res.writeHead(200,  {'Content-Type' : 'text/html'});
            res.end(templateFunction({
                ...data
            }));
            return;
        } else {
            const payload = await fileController(req);

            console.log(`200 OK ${req.url}`);
            res.writeHead(200,  { 'Content-Type': getContentType(path.extname(payload.file)) });
            res.end(payload.data);
            return;
        }
    } catch (err) {
        if (err.code === 'ENOENT') {
            const templateFunction = pug.compileFile(`${__dirname}/src/404.pug`);

            console.log(`404 Not Found ${req.url}`);
            res.writeHead(404,  { 'Content-Type': 'text/html' });
            res.end(templateFunction({}));
            return;
        }

        console.error(new Error(err))
    }

    res.writeHead(500);
    res.end('500 Server Error');
});

const port = argv.port || 3000;

server.listen(port);
console.log(`Server is now listening at http://localhost:${port}`)
