const fs = require('fs').promises
const path = require('path');

const directoryController = async function directoryController(req) {
    const file = path.resolve(path.join(process.cwd(), req.url));

    const dir = await fs.opendir(file);

    const entries = [];
    let entry = await dir.read();

    while (entry) {
        entries.push(entry);
        entry = await dir.read();
    }

    return {
        base: req.url,
        entries: entries.map((f) => f.name)
    };
}

module.exports = directoryController;