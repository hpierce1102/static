const path = require('path');
const fs = require('fs').promises;

const fileController = async function(req) {
    const file = path.resolve(path.join(process.cwd(), req.url));
    const data =  await fs.readFile(file);

    return {
        file,
        data
    }
}

module.exports = fileController;